epoch:0
avg_rwd:508.3924900863409
avg_steps:51.4

epoch:5
avg_rwd:462.1716531909193
avg_steps:46.8

epoch:10
avg_rwd:509.8714374152851
avg_steps:51.6

epoch:15
avg_rwd:572.5562626540197
avg_steps:57.9

epoch:20
avg_rwd:685.992813600247
avg_steps:69.3

epoch:25
avg_rwd:783.5874815172897
avg_steps:79.1

epoch:30
avg_rwd:660.391529321363
avg_steps:66.8

epoch:35
avg_rwd:658.673862260415
avg_steps:66.6

epoch:40
avg_rwd:734.4490017425185
avg_steps:74.2

epoch:45
avg_rwd:773.5759344555274
avg_steps:78.1

epoch:50
avg_rwd:879.5674220478508
avg_steps:88.7

epoch:55
avg_rwd:1189.4877332888557
avg_steps:119.7

epoch:60
avg_rwd:1023.5914094357165
avg_steps:103.1

epoch:65
avg_rwd:1148.7878756613463
avg_steps:115.6

epoch:70
avg_rwd:1167.6329779999846
avg_steps:117.5

epoch:75
avg_rwd:1137.4043631746367
avg_steps:114.5

epoch:80
avg_rwd:1104.2416056674413
avg_steps:111.2

epoch:85
avg_rwd:1492.2298869598908
avg_steps:150.0

epoch:90
avg_rwd:1390.7681113483684
avg_steps:139.9

epoch:95
avg_rwd:1360.701558017286
avg_steps:136.9

epoch:100
avg_rwd:1443.8563956441888
avg_steps:145.2

epoch:105
avg_rwd:1558.7328588098048
avg_steps:156.7

epoch:110
avg_rwd:1397.7482643049439
avg_steps:140.6

epoch:115
avg_rwd:1485.6715214594058
avg_steps:149.4

epoch:120
avg_rwd:1417.520024805402
avg_steps:142.6

epoch:125
avg_rwd:1285.7950793874488
avg_steps:129.4

epoch:130
avg_rwd:1250.634112502375
avg_steps:125.9

epoch:135
avg_rwd:1385.904838247054
avg_steps:139.4

epoch:140
avg_rwd:1502.7628212606373
avg_steps:151.1

epoch:145
avg_rwd:1682.3506362701314
avg_steps:169.1

epoch:150
avg_rwd:1708.0370301283838
avg_steps:171.6

epoch:155
avg_rwd:1977.4440807519186
avg_steps:198.2

epoch:160
avg_rwd:1825.5136247868654
avg_steps:182.8

epoch:165
avg_rwd:1686.5649921584743
avg_steps:168.8

epoch:170
avg_rwd:1596.7291418446684
avg_steps:159.8

epoch:175
avg_rwd:2037.6584896446325
avg_steps:203.9

epoch:180
avg_rwd:1872.5784441081337
avg_steps:187.4

epoch:185
avg_rwd:2079.3980366000874
avg_steps:208.1

epoch:190
avg_rwd:2120.115036186914
avg_steps:212.2

epoch:195
avg_rwd:1901.0054575732254
avg_steps:190.3

epoch:200
avg_rwd:1914.1934455905605
avg_steps:191.6

epoch:205
avg_rwd:1896.2945061804792
avg_steps:189.8

epoch:210
avg_rwd:1979.0536729593034
avg_steps:198.1

epoch:215
avg_rwd:1851.6551811246495
avg_steps:185.4

epoch:220
avg_rwd:1926.4363330157473
avg_steps:192.9

epoch:225
avg_rwd:2061.1805258696554
avg_steps:206.4

epoch:230
avg_rwd:1839.9280451274049
avg_steps:184.3

epoch:235
avg_rwd:1880.4587416417448
avg_steps:188.4

epoch:240
avg_rwd:1830.5935683161933
avg_steps:183.4

epoch:245
avg_rwd:1883.3398918734572
avg_steps:188.6

epoch:250
avg_rwd:1902.7898989638925
avg_steps:190.5

epoch:255
avg_rwd:1880.2201198247963
avg_steps:188.2

epoch:260
avg_rwd:1998.708114023027
avg_steps:200.0

epoch:265
avg_rwd:2122.954513141778
avg_steps:212.4

epoch:270
avg_rwd:2205.0437452832925
avg_steps:220.6

epoch:275
avg_rwd:2127.1203335386813
avg_steps:212.8

epoch:280
avg_rwd:2292.112298647445
avg_steps:229.3

epoch:285
avg_rwd:2900.9560815208138
avg_steps:290.2

epoch:290
avg_rwd:3255.624874151423
avg_steps:325.7

epoch:295
avg_rwd:9743.7413094021
avg_steps:974.5

epoch:300
avg_rwd:5417.3731801315025
avg_steps:541.9

epoch:305
avg_rwd:3389.567673400311
avg_steps:339.1

epoch:310
avg_rwd:9998.961443379923
avg_steps:1000.0

epoch:315
avg_rwd:9999.750439988944
avg_steps:1000.0

epoch:320
avg_rwd:9999.882195726015
avg_steps:1000.0