import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from runner import *
import os


def adjust_KpandKd(env, alpha):
    '''
    adjust environment's kp and kd for assistance torque on the root joint
    env: simluation environment
    alpha : shrinking factor
    '''
    kp = env.pdCon.init_kp.copy()
    env.setKpandKd(kp[2, 2]*alpha)


def PPO(save_path,
        env,
        actor,
        critic,
        s_norm,
        clip_param,
        ppo_epoch,
        batchsize,
        num_mini_batch,
        lr_actor=None,
        lr_critic=None,
        max_grad_norm=None,
        env_maxsteps=500,
        curriculum=True

        ):
    '''
    PPO optimzation algorithm
    env: simulation environment
    actor: actor neural network
    critic: critic network
    s_norm : state normalization module
    clip_param : clip parameters in PPO
    ppo_epoch : epochs of PPO optimization
    batchsize: number of samples sampled in an epoch
    num_mini_batch : batchsize
    lr_actor: learning rate of actor neural network
    lr_critic : learning rate of critic neural network
    max_grad_norm : clip parameters of max gradient norm
    env_maxsteps: max episode length of environment
    curriculum: whether use curriculum learning, only for bipedal controller environment

    '''
    # create folder to save model
    if(not os.path.isdir(save_path)):
        os.mkdir(save_path)

    #writer = SummaryWriter()

    runner = Runner(env, s_norm, actor, critic, batchsize, 0.99,
                    0.95, env_maxsteps)  # runner to collect data
    # actor neural network optimizer
    optimizer_actor = optim.Adam(actor.parameters(), lr=lr_actor)
    # critic neural network optimizer
    optimizer_critic = optim.Adam(critic.parameters(), lr=lr_critic)
    num_samples = ppo_epoch * runner.sample_size  # total samples
    num_sample = 0  # current samples

    if(curriculum):
        # important settings for curriculum learning
        # if reward is larger than this threshold, shrink the kp and kd of assistance torque
        rwd_threshold = 170
        full_assist_flag = True

    for i in range(ppo_epoch):
        num_sample += runner.sample_size
        rollouts = runner.run()  # get collected samples
        obs = rollouts["obs"]
        acs = rollouts["acs"]
        obs_next = rollouts["obs_next"]
        rwds = rollouts["rwds"]
        dones = rollouts["dones"]
        vtars = rollouts["vtars"]
        advs = rollouts["advs"]
        alogps = rollouts["alogps"]
        vpreds = rollouts["vpreds"]

        # normalize advantages
        advs = (advs-advs.mean())/(advs.std() + 0.0001)
        advs = np.clip(advs, -4, 4)

        value_loss_epoch = 0  # loss of critic network
        action_loss_epoch = 0  # loss of actor network
        symmetry_loss_epoch = 0  # loss of symmetry

        num_iter = int(obs.shape[0] / num_mini_batch)

        for iter_inner in range(10):

            id_list = np.arange(obs.shape[0])
            np.random.shuffle(id_list)  # shuffle data

            for idx in range(num_iter):
                idx_range = np.random.choice(
                    obs.shape[0], num_mini_batch, replace=False)
                obs_batch = torch.Tensor(obs[idx_range, :]).float()
                acs_batch = torch.Tensor(acs[idx_range, :]).float()
                vtars_batch = torch.Tensor(vtars[idx_range, :]).float()
                alogps_old_batch = torch.Tensor(alogps[idx_range, :]).float()
                advs_batch = torch.Tensor(advs[idx_range, :]).float()
                vpreds_old_batch = torch.Tensor(vpreds[idx_range, :]).float()

                optimizer_actor.zero_grad()
                optimizer_critic.zero_grad()

                obs_normed_batch = s_norm(obs_batch)
                m = actor.act_distribution(obs_normed_batch)
                alogps_batch = m.log_prob(acs_batch).sum(dim=1).view(-1, 1)
                vpreds_batch = critic(obs_normed_batch)
                ratio = torch.exp(alogps_batch - alogps_old_batch)

                # add your code here, compute objective function in PPO
                # hint: you can use torch.clamp() to clip objective functions
                objective = torch.min(
                    ratio*advs_batch, torch.clamp(ratio, 1-clip_param, 1+clip_param)*advs_batch).mean()
                #objective = torch.sum(objectivePreE)/len(objectivePreE)

                # print(objective)

                value_loss = (vtars_batch-vpreds_batch).pow(2).mean()

                # print(value_loss)

                objective = -objective
                value_loss.backward()
                objective.backward()

                nn.utils.clip_grad_norm_(actor.parameters(), max_grad_norm)
                nn.utils.clip_grad_norm_(critic.parameters(), max_grad_norm)

                optimizer_actor.step()
                optimizer_critic.step()

        num_updates = num_iter * num_mini_batch

        if(i % 10 == 0):
            data = {"actor": actor.state_dict(),
                    "critic": critic.state_dict(),
                    "s_norm": s_norm.state_dict()}

            torch.save(data, save_path+"/checkpoint_"+str(i)+".tar")
        if(i % 5 == 0):
            print("epoch:{}".format(i))
            rwd_acc, test_step = runner.testModel()
            if(curriculum):
                if(rwd_acc > rwd_threshold):
                    # shrink kp and kd
                    if(full_assist_flag == True):
                        full_assist_flag = False
                        rwd_threshold *= 0.8
                    adjust_KpandKd(runner.env, 0.75)
                print("kp_start:{}".format(runner.env.kpandkd_start))
                print("kp_end:{}".format(runner.env.kpandkd_end))
            print(" ")


def actor_critic(save_path,
                 env,
                 actor,
                 critic,
                 s_norm,
                 actor_critic_epoch,
                 batchsize,
                 num_mini_batch,
                 lr_actor=None,
                 lr_critic=None,
                 max_grad_norm=None,
                 env_maxsteps=500,
                 curriculum=True

                 ):
    '''
    simple actor critic optimzation algorithm
    env: simulation environment
    actor: actor neural network
    critic: critic network
    s_norm : state normalization module
    ppo_epoch : epochs of PPO optimization
    batchsize: number of samples sampled in an epoch
    num_mini_batch : batchsize
    lr_actor: learning rate of actor neural network
    lr_critic : learning rate of critic neural network
    max_grad_norm : clip parameters of max gradient norm
    env_maxsteps: max episode length of environment
    curriculum: whether use curriculum learning, only for bipedal controller environment

    '''
    # create folder to save model
    if(not os.path.isdir(save_path)):
        os.mkdir(save_path)
    runner = Runner(env, s_norm, actor, critic, batchsize, 0.99,
                    0.95, env_maxsteps)  # runner to collect data
    # actor neural network optimizer
    optimizer_actor = optim.Adam(actor.parameters(), lr=lr_actor)
    # critic neural network optimizer
    optimizer_critic = optim.Adam(critic.parameters(), lr=lr_critic)
    num_samples = actor_critic_epoch * runner.sample_size  # total samples
    num_sample = 0  # current samples

    if(curriculum):
        # important settings for curriculum learning
        # if reward is larger than this threshold, shrink the kp and kd of assistance torque
        rwd_threshold = 170
        full_assist_flag = True

    for i in range(actor_critic_epoch):
        num_sample += runner.sample_size
        rollouts = runner.run()  # get collected samples
        obs = rollouts["obs"]
        acs = rollouts["acs"]
        obs_next = rollouts["obs_next"]
        rwds = rollouts["rwds"]
        dones = rollouts["dones"]
        vtars = rollouts["vtars"]
        advs = rollouts["advs"]
        alogps = rollouts["alogps"]
        vpreds = rollouts["vpreds"]

        # normalize advantages
        advs = (advs-advs.mean())/(advs.std() + 0.0001)
        advs = np.clip(advs, -4, 4)

        value_loss_epoch = 0  # loss of critic network
        action_loss_epoch = 0  # loss of actor network

        num_iter = int(obs.shape[0] / num_mini_batch)

        for iter_inner in range(10):

            id_list = np.arange(obs.shape[0])
            np.random.shuffle(id_list)  # shuffle data

            for idx in range(num_iter):
                idx_range = np.random.choice(
                    obs.shape[0], num_mini_batch, replace=False)
                obs_batch = torch.Tensor(obs[idx_range, :]).float()
                acs_batch = torch.Tensor(acs[idx_range, :]).float()
                vtars_batch = torch.Tensor(vtars[idx_range, :]).float()
                alogps_old_batch = torch.Tensor(alogps[idx_range, :]).float()
                advs_batch = torch.Tensor(advs[idx_range, :]).float()
                vpreds_old_batch = torch.Tensor(vpreds[idx_range, :]).float()

                optimizer_actor.zero_grad()
                optimizer_critic.zero_grad()

                obs_normed_batch = s_norm(obs_batch)
                m = actor.act_distribution(obs_normed_batch)
                alogps_batch = m.log_prob(acs_batch).sum(dim=1).view(-1, 1)
                vpreds_batch = critic(obs_normed_batch)
                # compute value_loss
                # add your code here
                #Target - Predict
                value_loss = (vtars_batch-vpreds_batch).pow(2).mean()

                value_loss.backward()
                nn.utils.clip_grad_norm_(critic.parameters(), max_grad_norm)
                optimizer_critic.step()

                if(iter_inner == 0):
                        # compute your objective here to replace the line below
                    objective = None
                    objective = (alogps_batch * advs_batch).sum() / \
                        len(alogps_batch)

                    objective = -objective
                    nn.utils.clip_grad_norm_(actor.parameters(), max_grad_norm)
                    objective.backward()
                    optimizer_actor.step()

        num_updates = num_iter * num_mini_batch

        if(i % 10 == 0):
            data = {"actor": actor.state_dict(),
                    "critic": critic.state_dict(),
                    "s_norm": s_norm.state_dict()}

            torch.save(data, save_path+"/checkpoint_"+str(i)+".tar")
        if(i % 5 == 0):
            print("epoch:{}".format(i))
            rwd_acc, test_steps = runner.testModel()
            if(curriculum):
                if(rwd_acc > rwd_threshold):
                    # shrink kp and kd
                    if(full_assist_flag == True):
                        full_assist_flag = False
                        rwd_threshold *= 0.8
                    adjust_KpandKd(runner.env, 0.75)
                print("kp_start:{}".format(runner.env.kpandkd_start))
                print("kp_end:{}".format(runner.env.kpandkd_end))
            print(" ")


def reinforce(save_path,
              env,
              actor,
              critic,
              s_norm,
              actor_epoch,
              batchsize,
              num_mini_batch,
              lr_actor=None,
              max_grad_norm=None,
              env_maxsteps=500,
              curriculum=True
              ):
    '''
    simple actor critic optimzation algorithm
    env: simulation environment
    actor: actor neural network
    critic: critic network
    s_norm : state normalization module
    ppo_epoch : epochs of PPO optimization
    batchsize: number of samples sampled in an epoch
    num_mini_batch : batchsize
    lr_actor: learning rate of actor neural network
    lr_critic : learning rate of critic neural network
    max_grad_norm : clip parameters of max gradient norm
    env_maxsteps: max episode length of environment
    curriculum: whether use curriculum learning, only for bipedal controller environment

    '''
    # create folder to save model
    if(not os.path.isdir(save_path)):
        os.mkdir(save_path)
    runner = Runner(env, s_norm, actor, critic, batchsize, 0.99,
                    0.95, env_maxsteps)  # runner to collect data
    # actor neural network optimizer
    optimizer_actor = optim.Adam(actor.parameters(), lr=lr_actor)
    num_samples = actor_epoch * runner.sample_size  # total samples
    num_sample = 0  # current samples

    if(curriculum):
        # important settings for curriculum learning
        # if reward is larger than this threshold, shrink the kp and kd of assistance torque
        rwd_threshold = 170
        full_assist_flag = True

    for i in range(actor_epoch):
        num_sample += runner.sample_size
        rollouts = runner.run()  # get collected samples
        obs = rollouts["obs"]
        acs = rollouts["acs"]
        obs_next = rollouts["obs_next"]
        rwds = rollouts["rwds"]
        dones = rollouts["dones"]
        advs = rollouts["advs"]
        alogps = rollouts["alogps"]
        acc_rwds = rollouts["acc_rwds"]

        action_loss_epoch = 0  # loss of actor network

        num_iter = int(obs.shape[0] / num_mini_batch)

        for iter_inner in range(1):

            id_list = np.arange(obs.shape[0])
            np.random.shuffle(id_list)  # shuffle data

            for idx in range(num_iter):
                idx_range = np.random.choice(
                    obs.shape[0], num_mini_batch, replace=False)
                obs_batch = torch.Tensor(obs[idx_range, :]).float()
                acs_batch = torch.Tensor(acs[idx_range, :]).float()
                alogps_old_batch = torch.Tensor(alogps[idx_range, :]).float()
                advs_batch = torch.Tensor(advs[idx_range, :]).float()
                acc_rwds_batch = torch.Tensor(acc_rwds[idx_range, :]).float()

                optimizer_actor.zero_grad()

    # mb_obs=[] #observation list
    # mb_acs=[] # actions list
    # mb_rwds=[] # rewaid list
    # mb_vpreds=[] #predicted values list
    # mb_alogps=[] #log probability of action list
    # mb_obs_next=[] # next observation list
    # mb_dones=[] # dones list
    # mb_vpreds_next=[] # next predicted value list
    #mb_fails = []

                # The big function is created from two arrays
                # Use torch.multiply to multiply to multiply the two vectors together
                # Then can use torch.sum to get a scalar value which we can assign to the objective
                # Use tpye to determine the data type

                # Literaelly have no idea what this does
                # Do I use it? Or not use it
                obs_normed_batch = s_norm(obs_batch)
                m = actor.act_distribution(obs_normed_batch)
                alogps_batch = m.log_prob(acs_batch).sum(dim=1).view(-1, 1)

                # Its just doing randoms steps and I dont know why

                # print(alogps_batch)
                objectiveScal = (
                    (alogps_batch * acc_rwds_batch).sum())/len(acc_rwds_batch)
                # print(acc_rwds_batch)
                # print(objectiveScal)

                objective = objectiveScal
                # print(objective)
                # Call a sum function to generate the scalar for objective

                objective = - objective
                # Back propagation on objective
                objective.backward()
                nn.utils.clip_grad_norm_(actor.parameters(), max_grad_norm)
                # Maps NN to objective
                optimizer_actor.step()

        num_updates = num_iter * num_mini_batch

        if(i % 10 == 0):
            data = {"actor": actor.state_dict(),
                    "critic": critic.state_dict(),
                    "s_norm": s_norm.state_dict()}

            torch.save(data, save_path+"/checkpoint_"+str(i)+".tar")
        if(i % 5 == 0):
            print("epoch:{}".format(i))
            rwd_acc, test_steps = runner.testModel()
            if(curriculum):
                if(rwd_acc > rwd_threshold):
                    # shrink kp and kd
                    if(full_assist_flag == True):
                        full_assist_flag = False
                        rwd_threshold *= 0.8
                adjust_KpandKd(runner.env, 0.75)
                print("kp_start:{}".format(runner.env.kpandkd_start))
                print("kp_end:{}".format(runner.env.kpandkd_end))
            print(" ")
